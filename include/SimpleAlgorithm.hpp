/* ----------------------------------------------------------------*\
    Application
        SimpleAlgorithm

    Description
        2D-solver for steady-state incompressible flow using 
        the SIMPLE algorithm (Patankar and Spalding, 1972).
        Simple rectangular mesh only for the moment.

    SIMPLE algorithm (iterative method)
        Step 1: Guess a pressure field P*.
        Step 2: Solve the momentum equations for U*, V* based on P*.
        Step 3: Solve the pressure correction equation for P', then
                compute the corrected pressure P = P* + P'.
        Step 4: Calculate u, v using velocity correction formulas.
        Step 5: Solve other flow field properties equations
                (temperature, viscosity, turbulence).
        Step 5: Treat P as the new P* and return to step 2 until
                convergence or maximum iteration is reached.
\* ----------------------------------------------------------------*/

#include <unistd.h>
#include <iostream>
#include <math.h>
#include <cstring>
#include <cblas.h>
#include "Functions.hpp"

#define SIMPLE_ALGORITHM_VERBOSE

namespace ColverT {
/* 
 * @brief Initialize the memory for the Simple Algorithm
 */
void SimpleAlgorithm_Initialize(size_t gridSize, double*** aw, double*** ae, double*** an, double*** as, 
                                double*** apu, double*** apv, double*** app);

void SimpleAlgorithm_InitializeOutput(size_t gridSize, double*** U, double*** V, double*** P, double*** Uold, double*** Vold, double*** Pold);

int SimpleAlgorithm(const size_t maxIterations, const double Xmax, const double Ymax, const size_t gridSize, 
                    double **Uoutput, double **Voutput, double **PressureOutput);


}