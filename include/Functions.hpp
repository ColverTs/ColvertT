/* ----------------------------------------------------------------*\
    Application
        Functions

    Description
        A compilation of general functions such as dot vector, or 
        max of 3 elements. Some of these are just wrappers, created 
        to facilitate the use and optimization.
\* ----------------------------------------------------------------*/
//  int types
#include <cstdint>
//  size_t
#include <cstddef>
//  max function
#include <algorithm>
//  BLAS functions
#include <cblas.h>

namespace ColverT {
    namespace Functions {
        inline double Max3(const double a, const double b, const double c) {
            return std::max(a,std::max(b,c));
        }

        inline double SumMagnitudes(const size_t size_vector, double* vector) {
            return cblas_dasum( size_vector, vector, 1);
        }

        inline double DotProduct(const size_t size_vectors, double* vector_1, double* vector_2) {
            return  cblas_ddot( size_vectors , vector_1 , 1 , vector_2 , 1);
        }

        inline double EuclideanNorm(size_t size_vector, double* vector) {
            return cblas_dnrm2( size_vector, vector, 1);
        }

        inline void LinearCombination(size_t size_vectors, double* vector_1, double alpha, double* vector_2) {
            cblas_daxpy( size_vectors, 
                        alpha, vector_2, 1,
                                vector_1, 1);
        }

    }
}