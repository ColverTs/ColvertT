

CXX=g++
#INCLUDE=$(addprefix -I,include) $(addprefix -L, lib) -Wl,-rpath=lib
INCLUDE=$(addprefix -I, /opt/OpenBLAS/include) $(addprefix -L, /opt/OpenBLAS/lib)
CXXFLAGS=-Wall -O3
LDLIBS=-lm -lopenblas


main: src/main.cpp build/SimpleAlgorithm.o build/SimpleAlgorithm_Initialize.o build/Functions.o
	$(CXX) $(CXXFLAGS) $(INCLUDE) build/SimpleAlgorithm.o build/SimpleAlgorithm_Initialize.o build/Functions.o $< -o $@ $(LDLIBS)


build/SimpleAlgorithm_Initialize.o: src/SimpleAlgorithm_Initialize.cc include/SimpleAlgorithm.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@

build/SimpleAlgorithm.o: src/SimpleAlgorithm.cc include/SimpleAlgorithm.hpp build/SimpleAlgorithm_Initialize.o build/Functions.o
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDLIBS)



# ======================================== GENERAL FUNCTIONS ========================================
FUNCTIONS_O = $(addprefix build/, $(addsuffix .o, DotProduct LinearCombination Max3 SumMagnitudes))
build/%.o: src/Functions/%.cc include/Functions.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDLIBS)

#The -r option tells ld to perform a relink, which combines the object files into a single file
# build/Functions.o: $(FUNCTIONS_O)
# 	ld -r $(FUNCTIONS_O) -o $@
build/Functions.o: src/Functions/DotProduct.cc include/Functions.hpp
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDLIBS)
# ====================================== END GENERAL FUNCTIONS ======================================

clean:
	rm -f main build/*.o