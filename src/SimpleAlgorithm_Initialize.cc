#include "SimpleAlgorithm.hpp"


namespace ColverT {
/* 
 * @brief Initialize the memory for the Simple Algorithm
 */
void SimpleAlgorithm_Initialize(size_t gridSize, double*** aw, double*** ae, double*** an, double*** as, 
                                double*** apu, double*** apv, double*** app) {
// ==== Initialisation ====
// Notations are the same as in Patannkar (1972) and Versteeg (1995)
// Discretised coefficients matrices of Navier-Stokes momentum equations
    *aw = (double**) malloc((gridSize+2) * sizeof(double*));
    *ae = (double**) malloc((gridSize+2) * sizeof(double*));
    *an = (double**) malloc((gridSize+2) * sizeof(double*));
    *as = (double**) malloc((gridSize+2) * sizeof(double*));
    *apu = (double**) malloc((gridSize+2) * sizeof(double*));
    *apv = (double**) malloc((gridSize+2) * sizeof(double*));
    *app = (double**) malloc((gridSize+2) * sizeof(double*));
    for (unsigned int i = 0; i < gridSize+2; i++) {
        (*aw)[i] = (double*) calloc((gridSize+2), sizeof(double));
        (*ae)[i] = (double*) calloc((gridSize+2), sizeof(double));
        (*an)[i] = (double*) calloc((gridSize+2), sizeof(double));
        (*as)[i] = (double*) calloc((gridSize+2), sizeof(double));

        (*apu)[i] = (double*) malloc((gridSize+2) * sizeof(double));
        (*apv)[i] = (double*) malloc((gridSize+2) * sizeof(double));
        (*app)[i] = (double*) malloc((gridSize+2) * sizeof(double));
        for (unsigned int j = 0; j < gridSize+2; j++) {
            (*apu)[i][j] = 1.0;
            (*apv)[i][j] = 1.0;
            (*app)[i][j] = 1.0;
        }
    }
}

void SimpleAlgorithm_InitializeOutput(size_t gridSize, double*** U, double*** V, double*** P, double*** Uold, double*** Vold, double*** Pold) {
// Unknown matrices to calculate for current and previous time steps (X-velocity U, Y-velocity V, pressure P)
    *U = (double**) malloc((gridSize+2)*sizeof(double*));
    *V = (double**) malloc((gridSize+2)*sizeof(double*));
    *P = (double**) malloc((gridSize+2)*sizeof(double*));
    *Uold = (double**) malloc((gridSize+2)*sizeof(double*));
    *Vold = (double**) malloc((gridSize+2)*sizeof(double*));
    *Pold = (double**) malloc((gridSize+2)*sizeof(double*));

    (*U)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    (*V)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    (*P)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    (*Uold)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    (*Vold)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    (*Pold)[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));

// ==== Boundary conditions (Dirichlet-type) ====
// For a lid-driven cavity case: moving top boundary (Utop = U0) and three walls (Uwall = 0)
    (*U)[0][gridSize+1] = 1.0;
    (*Uold)[0][gridSize+1] = 1.0;
    for (unsigned int m = 1; m < gridSize+2; m++) {
        (*U)[m] = (double*) &((*U)[0][m*(gridSize+2)]);
        (*V)[m] = (double*) &((*V)[0][m*(gridSize+2)]);
        (*P)[m] = (double*) &((*P)[0][m*(gridSize+2)]);
        (*Uold)[m] = (double*) &((*Uold)[0][m*(gridSize+2)]);
        (*Vold)[m] = (double*) &((*Vold)[0][m*(gridSize+2)]);
        (*Pold)[m] = (double*) &((*Pold)[0][m*(gridSize+2)]);
    // Top boundary
        (*U)[m][gridSize+1] = 1.0;
        (*Uold)[m][gridSize+1] = 1.0;
    }

    for (unsigned int m = 0; m < gridSize+2; m++) {
        for (unsigned int j = 0; j < gridSize+2; j++) {
            (*P)[m][j] = 1.0;
            (*Pold)[m][j] = 1.0;
        }
    }
}

}