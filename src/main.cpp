/* ----------------------------------------------------------------*\
    Application
        simpleColverT2D

    Description
        2D-solver for steady-state incompressible flow using 
        the SIMPLE algorithm (Patankar and Spalding, 1972).
        Simple rectangular mesh only for the moment.

    SIMPLE algorithm (iterative method)
        Step 1: Guess a pressure field P*.
        Step 2: Solve the momentum equations for U*, V* based on P*.
        Step 3: Solve the pressure correction equation for P', then
                compute the corrected pressure P = P* + P'.
        Step 4: Calculate u, v using velocity correction formulas.
        Step 5: Solve other flow field properties equations
                (temperature, viscosity, turbulence).
        Step 5: Treat P as the new P* and return to step 2 until
                convergence or maximum iteration is reached.
\* ----------------------------------------------------------------*/

#include <iostream>
#include <math.h>
#include <cstring>
#include "SimpleAlgorithm.hpp"
#include <chrono>


int main() {

    // ==== Writing data file ====
    // Copied and slightly modified from a git repo, optimise it however the fuck you want
	FILE *dataFile;
	dataFile = fopen("results.dat", "w");

	if (dataFile == NULL) {
        printf("\nERROR when opening file!\n");
        return -1;
	}

    // Matrices for the output
    double **Uoutput, **Voutput, **PressureOutput;
    const double Xmax = 4e-4; // Length
    const double Ymax = 4e-4; // Height
    unsigned int gridSize = 100;
    Uoutput = (double**) malloc((gridSize+2)*sizeof(double*));
    Voutput = (double**) malloc((gridSize+2)*sizeof(double*));
    PressureOutput = (double**) malloc((gridSize+2)*sizeof(double*));
    for (unsigned int j = 0; j<gridSize+2; j++) {
        Uoutput[j] = (double*) malloc((gridSize+2)*sizeof(double));
        Voutput[j] = (double*) malloc((gridSize+2)*sizeof(double));
        PressureOutput[j] = (double*) malloc((gridSize+2)*sizeof(double));
    }

    auto start = std::chrono::steady_clock::now();
    int convergence = ColverT::SimpleAlgorithm(5000, Xmax, Ymax, gridSize, Uoutput, Voutput, PressureOutput);
    auto end = std::chrono::steady_clock::now();
    std::cout<< "Time :" << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()<<" ms\n";

    if (convergence != 0) {
        std::cout << "Writing solutions...\n";
        fprintf(dataFile, "VARIABLES=\"X\",\"Y\",\"U\",\"V\",\"P\"\n");
        fprintf(dataFile, "Xi=%d, Yi=%d\n", gridSize, gridSize);


    //  TODO : yucky
        const double dx = Xmax/(double)gridSize; 
        const double dy = Ymax/(double)gridSize;

        for (unsigned int j = 0; j <= gridSize; j++) {
            for (unsigned int i = 0 ; i <= gridSize; i++) {
                double xpos, ypos;
                xpos = i*dx;
                ypos = j*dy;
                fprintf(dataFile, "%5.8lf\t%5.8lf\t%5.8lf\t%5.8lf\t%5.8lf\n", xpos, ypos, Uoutput[i][j], Voutput[i][j], PressureOutput[i][j]);
            }
        }
        
        fclose(dataFile);

        std::cout << "Solutions are written in results.dat!\n";
    }
}