#include "SimpleAlgorithm.hpp"

namespace ColverT {

int SimpleAlgorithm(const size_t maxIterations, const double Xmax, const double Ymax, size_t gridSize, 
                    double **Uoutput, double **Voutput, double **PressureOutput) {
// ==== Variables ====
// Notations are the same as in Patannkar (1972) and Versteeg (1995)
// Discretised coefficients matrices of Navier-Stokes momentum equations
    double **aw, **ae, **an, **as;
    double **apu, **apv, **app;
    ColverT::SimpleAlgorithm_Initialize(gridSize, &aw, &ae, &an, &as, &apu, &apv, &app);
// Variables for calculation results    
/*    double massBalance[gridSize+2][gridSize+2], PressureCorrection[gridSize+2][gridSize+2];
    for (int i = 0; i <= gridSize+1; i++) {
        for (int j = 0; j <= gridSize+1; j++) {
            PressureCorrection[i][j] = 0.0;
            massBalance[i][j] = 0.0;
        }
    }*/

    double **massBalance, **PressureCorrection;
    massBalance = (double**) malloc((gridSize+2)*sizeof(double*));
    PressureCorrection = (double**) malloc((gridSize+2)*sizeof(double*));
    massBalance[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    PressureCorrection[0] = (double*) calloc((gridSize+2)*(gridSize+2), sizeof(double));
    for (unsigned int i = 1; i < gridSize+2; i++) {
        massBalance[i] = &(massBalance[0][i*(gridSize+2)]);
        PressureCorrection[i] = &(PressureCorrection[0][i*(gridSize+2)]);
    }

    double totalMassBalance, fe, fw, fn, fs, de, dw, dn, ds;
    double residualU, residualV, residualP;

// Unknown matrices to calculate for current and previous time steps (X-velocity U, Y-velocity V, pressure P)
    double **U, **V, **P, **Uold, **Vold, **Pold;
    ColverT::SimpleAlgorithm_InitializeOutput(gridSize, &U, &V, &P, &Uold, &Vold, &Pold);


// ==== Parameters for calculations (User inputs) ====
// Dimensions of each cell
    unsigned int numberOfCells = gridSize*gridSize;
    const double dx = Xmax/(double)gridSize; 
    const double dy = Ymax/(double)gridSize;
// Relaxation factors
    const double alphaP = 0.1;   
    const double alphaU = 0.6;   
    const double alphaV = 0.6;   
// Iteration "Run calculation" variables
    const size_t velociyMaxIter = 10;
    const size_t pressureMaxIter = 100;
// Variables for convergence check
    const double convergenceU = 1e-5;            // Convergence criteria for X-velocity U
    const double convergenceV = 1e-5;            // Convergence criteria for Y-velocity V
    const double convergenceP = 1e-5;            // Convergence criteria for pressure P
    const double convergenceContinuity = 1e-5;   // Convergence criteria for the continuity equation (conservation of mass)
    const int displayInfosCount = 100;           // Display calculation infos every n iterations
// Fluid properties
    const double dynamicViscosity = 1e-3;  
    const double density = 997;            

// ==== Initialisation ====

// ==== Main calculation loop ====
    size_t calculationIter;
    for (calculationIter=0;  calculationIter < maxIterations; calculationIter++) {
    // == Solve the U-momentum equation ==
    // coefficients for internal cells
        for(unsigned int i = 2; i <= gridSize; i++) {
            for(unsigned int j = 1; j <= gridSize; j++) {
                fe = density*dy*0.5*(Uold[i+1][j] + Uold[i][j]);
                fw = density*dy*0.5*(Uold[i][j] + Uold[i-1][j]);
                fn = density*dx*0.5*(Vold[i][j+1] + Vold[i-1][j+1]);
                fs = density*dx*0.5*(Vold[i][j] + Vold[i-1][j]);
                de = dynamicViscosity*(dy/dx);
                dw = dynamicViscosity*(dy/dx);
                dn = dynamicViscosity*(dx/dy);
                ds = dynamicViscosity*(dx/dy);
            // Hybrid differencing for discretisation
            // This is devised for a 1D method by Patankar. Extending to a 2D method works to some extent
                ae[i][j] = Functions::Max3(-fe, de - 0.5*fe, 0.0);
                aw[i][j] = Functions::Max3(fw, dw + 0.5*fw, 0.0);
                an[i][j] = Functions::Max3(-fn, dn - 0.5*fn, 0.0);
                as[i][j] = Functions::Max3(fs, ds + 0.5*fs, 0.0);
                apu[i][j] = ae[i][j] + aw[i][j] + an[i][j] + as[i][j]+ (fe-fw) + (fn-fs);
                apu[i][j] = apu[i][j]/alphaU;
            }
        }
        for(unsigned int velocityIter = 1; velocityIter <= velociyMaxIter; velocityIter++){
            for(unsigned int i = 2; i <= gridSize; i++){
                for(unsigned int j = 1; j <= gridSize; j++){
                    U[i][j] = (1.0 - alphaU) * Uold[i][j] + (1./apu[i][j])*(ae[i][j]*U[i+1][j] + aw[i][j]*U[i-1][j] + an[i][j]*U[i][j+1] + as[i][j]*U[i][j-1] + (P[i-1][j] - P[i][j])*dy);
                }
            }
        }

    /* Compute residual for U (FAILED)
        residualU = 0.0;
        scaleFactorU = 0.0;
        for (int i = 1; i <= gridSize; i++) {
            for (int j = 1; j <= gridSize; j++) {
                residualU = residualU + fabs(aw[i][j]*U[i][j-1] + ae[i][j]*U[i][j+1] + an[i][j]*U[i+1][j] + as[i][j]*U[i-1][j] - apu[i][j]*U[i][j]);
                scaleFactorU = scaleFactorU + fabs(apu[i][j]*U[i][j]);
            }
        }
        residualU = residualU/scaleFactorU;
    */

    // == Solve the V-momentum equations ==
    // Coefficients for internal cells
        for(unsigned int i = 1; i <= gridSize; i++) {
            for(unsigned int j = 2; j <= gridSize; j++) {
                fe = density*dy*0.5*(Uold[i+1][j] + Uold[i+1][j-1]);
                fw = density*dy*0.5*(Uold[i][j] + Uold[i][j-1]);
                fn = density*dx*0.5*(Vold[i][j] + Vold[i][j+1]);
                fs = density*dx*0.5*(Vold[i][j] + Vold[i][j-1]);
                de = dynamicViscosity*(dy/dx);
                dw = dynamicViscosity*(dy/dx);
                dn = dynamicViscosity*(dx/dy);
                ds = dynamicViscosity*(dx/dy);
            // Hybrid differencing for discretisation
                ae[i][j] = Functions::Max3(-fe, de - 0.5*fe, 0.0);
                aw[i][j] = Functions::Max3(fw, dw + 0.5*fw, 0.0);
                an[i][j] = Functions::Max3(-fn, dn - 0.5*fn, 0.0);
                as[i][j] = Functions::Max3(fs, ds + 0.5*fs, 0.0);
                apv[i][j] = ae[i][j] + aw[i][j] + an[i][j] + as[i][j] + (fe-fw) + (fn-fs);
                apv[i][j] = apv[i][j]/alphaV;
            }
        }
        for(unsigned int  velocityIter = 1; velocityIter <= velociyMaxIter; velocityIter++) {
            for(unsigned int i = 1; i <= gridSize; i++) {
                for(unsigned int j = 2; j <= gridSize; j++) {
                    V[i][j] = (1.0 - alphaV)*Vold[i][j] + (1.0/apv[i][j])*(ae[i][j]*V[i+1][j] + aw[i][j]*V[i-1][j] + an[i][j]*V[i][j+1] + as[i][j]*V[i][j-1] + (P[i][j-1] - P[i][j])*dx);
                }
            }
        }
    // == Solve the pressure correction equation ==
        for(unsigned int i = 1; i <= gridSize; i++) {
            for(unsigned int j = 1; j <= gridSize; j++){
                ae[i][j] = (density*dy*dy)*(1./apu[i+1][j]);
                aw[i][j] = (density*dy*dy)*(1./apu[i][j]);
                an[i][j] = (density*dy*dy)*(1./apv[i][j+1]);
                as[i][j] = (density*dy*dy)*(1./apv[i][j]);
            }
        }
    // Boundary values for pressure coefficients
        for(unsigned int j = 0; j <= gridSize+1; j++) {
            aw[1][j] = 0.0;
            ae[gridSize][j] = 0.0;
        }
        for(unsigned int i = 0; i <= gridSize+1; i++) {
            an[i][gridSize] = 0.0;
            as[i][1] = 0.0;
        }
        for(unsigned int i =0; i <= gridSize+1; i++) {
            for(unsigned int j = 0; j <= gridSize+1; j++){
                app[i][j] = ae[i][j] + aw[i][j] + an[i][j] + as[i][j];
            }
        }
        app[1][1] = 1*1e30;

    // Compute mass balance (continuity equation residual)
        for(unsigned int i = 1; i <= gridSize; i++) {
            for(unsigned int j = 1; j <= gridSize; j++) {
                massBalance[i][j] = density*dy*(U[i+1][j]-U[i][j]) + density*dx*(V[i][j+1]-V[i][j]);
//                massBalanceSum += massBalance[i][j]*massBalance[i][j];
            }
        }
        totalMassBalance = Functions::EuclideanNorm( (gridSize+2)*(gridSize+2), (double*) massBalance[0]);

    // Compute the pressure correction    
        for(unsigned int  pressureIter = 1; pressureIter <= pressureMaxIter; pressureIter++){
            for(unsigned int j = 1; j <= gridSize; j++) {
                for(unsigned int i = 1; i <= gridSize; i++){
                    PressureCorrection[i][j] += (1.7/app[i][j])*(ae[i][j]*PressureCorrection[i+1][j] + aw[i][j]*PressureCorrection[i-1][j] + an[i][j]*PressureCorrection[i][j+1] + as[i][j]*PressureCorrection[i][j-1] - massBalance[i][j] - PressureCorrection[i][j]*app[i][j]);
                }
            }
        }
    // == Correct pressure and velocities ==
//  P <- P + alphaP*PressureCorrection
//  PressureCorrection[0][j]... == 0.
//  This is possible because of the conditions to the limits
/*        cblas_daxpy( (gridSize+2)*(gridSize+2), 
                    alphaP, (double*) PressureCorrection[0], 1,
                    (double*) P, 1);*/
        for(unsigned int i = 1; i <= gridSize; i++) {
            for(unsigned int j = 1; j <= gridSize; j++) {
                P[i][j] = P[i][j] + alphaP*PressureCorrection[i][j];
            }
        }
        for(unsigned int i = 2; i <= gridSize; i++) {
            for(unsigned int j = 1; j <= gridSize; j++) {
                U[i][j] = U[i][j] + (dy/apu[i][j])*(PressureCorrection[i-1][j] - PressureCorrection[i][j]);
            }
        }
        for(unsigned int i = 1; i <= gridSize; i++) {
            for(unsigned int j = 2; j <= gridSize; j++) {
                 V[i][j] = V[i][j] + (dx/apv[i][j])*(PressureCorrection[i][j-1] - PressureCorrection[i][j]);
            }
        }
    // == Compute residuals ==
    // residual = sum_{i}(|Uold[i]-U[i]|)/ number_of_cells
//  This is possible because of the conditions to the limits
        Functions::LinearCombination( (gridSize+2)*(gridSize+2), 
                    (double*) Uold[0], -1.,
                    (double*) U[0]); // Uold <- Uold-U*/
        residualU = Functions::SumMagnitudes( (gridSize+2)*(gridSize+2), (double*) Uold[0]); 
//  TODO : V != 0.
        Functions::LinearCombination( (gridSize+2)*(gridSize+2), 
                    (double*) Vold[0], -1.,
                    (double*) V[0]); // Vold <- Vold-V*/
        residualV = Functions::SumMagnitudes( (gridSize+2)*(gridSize+2), (double*) Vold[0]); 
        Functions::LinearCombination( (gridSize+2)*(gridSize+2), 
                    (double*) Pold[0], -1.,
                    (double*) P[0]); // Pold <- Pold-P*/
        residualP = Functions::SumMagnitudes( (gridSize+2)*(gridSize+2), (double*) Pold[0]);
        residualU = residualU/numberOfCells;
        residualV = residualV/numberOfCells;
        residualP = residualP/numberOfCells;


    // == Display calculation informations == 
        if (calculationIter%displayInfosCount == 0) {
            std::cout << "Iter " << calculationIter << "\t" << residualU << "\t" << residualV << "\t" << residualP << "\t" << totalMassBalance << "\n";
        }
    // == Check convergence ==
    // Display order: Iteration number, U, V, P, Continuity
        if (residualU <= convergenceU && residualV <= convergenceV && residualP <= convergenceP 
            && totalMassBalance <= convergenceContinuity) {
            goto converged;
        }
    // == Copy U, V, P into Uold, Vold, Pold ==
        memcpy(Uold[0], U[0], (gridSize+2)*(gridSize+2)*sizeof(double));
        memcpy(Vold[0], V[0], (gridSize+2)*(gridSize+2)*sizeof(double));
        memcpy(Pold[0], P[0], (gridSize+2)*(gridSize+2)*sizeof(double));
    }

    std::cout<<"Did not converge.\n";
    return 0;

//  TODO : clear the memory
    converged:
        // ==== This part is not fully finished. This is a simplified method for the sake of post-processing ====
        for (unsigned int i = 1; i <= gridSize; i++) {
            for (unsigned int j = 1; j <= gridSize; j++) {
                Uoutput[i][j] = 0.5*(U[i][j] + U[i][j+1]);
                Voutput[i][j] = 0.5*(V[i][j] + V[i+1][j]);
                PressureOutput[i][j] = 0.25*(P[i][j] + P[i+1][j] + P[i][j+1] + P[i+1][j+1]);
            }
	    }
        std::cout<<"Converged in "<<calculationIter<<".\n";
        return calculationIter;

}

}