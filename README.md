# ColverT
ColverT is one of the worst solver for fluid dynamics out there. 


## Dependencies
ColverT relies on a few libraries : 
 * Apptainer : to be easily usable on Windows
 * OpenBLAS : to be speed.
You should (theoretically) only worry about getting Apptainer, as the rest of the libraries will be downloaded in the container.

## How to use
In your ColverT folder : 
```console
apptainer build ColverT.sif ColverT_definition_file.def
apptainer run ColverT.sif 
```
These commands respectively create an Apptainer container, and run it.



## FAQ
### Why doesn't it use floats even though the name is ColverT?
Because our mental health is sinking. Fast.