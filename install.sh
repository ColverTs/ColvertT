#!/usr/bin/bash 


# OpenBLAS
git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS
# We install OpenBLAS localy, to not overwrite the system's default BLAS
make
make install PREFIX=$PWD/..
cd ..